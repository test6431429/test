const pool = require("../database/db");

const {
    procedurePausaGameUsuario,
    queryGetPregunta
} = require("../services/pausaGameUsuarioService");

const getPausasActivas = async (req, res) => {
    try {
        const { idUsuario } = req.params;

        const promisePool = pool.promise();
        const [rows, fields] = await promisePool.query(procedurePausaGameUsuario, [
            'get_pausas_activas', idUsuario
        ]);

        res.json(rows[0]);
    } catch (error) {
        return res.status(500).json({ message: "Something goes wrong" });
    }
};

const getPausaGames = async (req, res) => {
    try {
        const { idUsuario } = req.params;

        const promisePool = pool.promise();
        const [rows, fields] = await promisePool.query(procedurePausaGameUsuario, [
            'get-pausa-games', idUsuario
        ]);

        res.json(rows[0]);
    } catch (error) {
        return res.status(500).json({ message: "Something goes wrong" });
    }
};

const getRespuestasGame = async (req, res) => {
    try {
        const { idGame } = req.params;

        const promisePool = pool.promise();
        const [rows] = await promisePool.query(queryGetPregunta, [ idGame ]);

        res.json(rows);
    } catch (error) {
        return res.status(500).json({ message: "Something goes wrong" });
    }
};

const saveRespuestaCorrecta = async (req, res) => {
    try {
        const { id } = req.body;

        const promisePool = pool.promise();
        const [rows] = await promisePool.query(procedurePausaGameUsuario, [
            'respuesta_correcta', id
        ])

        res.json(rows[0]);
        
    } catch (error) {
        
    }
}

const terminoPausa = async (req, res) => {
    try {
        const { id } = req.body;

        const promisePool = pool.promise();
        const [rows] = await promisePool.query(procedurePausaGameUsuario, [
            'termino_pausa_activa', id
        ])

        res.json(rows[0]);
        
    } catch (error) {
        return res.status(500).json({ message: "Something goes wrong" });
    }
}

module.exports = {
    getPausasActivas,
    getPausaGames,
    terminoPausa,
    getRespuestasGame,
    saveRespuestaCorrecta
};