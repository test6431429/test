const pool = require("../database/db");

const { queryGetPermissionsByPerfil } = require("../services/permissionService");

const getPermissionByPerfil = async (req, res) => {
    const { idPerfil } = req.params;
    try {

        const promisePool = pool.promise();

        const [rows] = await promisePool.query(queryGetPermissionsByPerfil, [idPerfil]);

        res.status(200).json(rows); //OK
        // res.json(permissions.rows);
    } catch (err) {
        console.error(err.message);
        res.status(err?.status || 500)
            .json({
                status: "fail",
                success: false,
                message: "No se pueden leer los permisos!!",
            });
    }
};

module.exports = {
    getPermissionByPerfil
};