const pool = require("../database/db");

const { queryGetRandomFrase } = require("../services/fraseService");

const getRandomFrase = async (req, res) => {
    try {
        const promisePool = pool.promise();

        const [row] = await promisePool.query(queryGetRandomFrase);

        return res.status(200).json(row[0]);
    } catch (err) {
        return res.status(err?.status || 500).json({
            status: "fail",
            success: false,
            message: "No se pudo leer los datos de la base dedatos!!",
        });
    }
};

module.exports = {
    getRandomFrase
};