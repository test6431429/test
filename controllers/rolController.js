const pool = require("../database/db");

const { queryRolesBySector } = require("../services/rolService");

const getRolesByPerfil = async (req, res) => {
    const { idSector } = req.params;
    try {
        const promisePool = pool.promise();

        const [rows] = await promisePool.query(queryRolesBySector, [idSector]);

        // res.status(200).send(req.body); //OK
        // console.log(rows);
        res.json(rows);
    } catch (err) {
        console.error(err.message);
        res.status(err?.status || 500)
            .send({
                status: "Fallo",
                data: { error: "No se pudo leer los roles de la base de datos", },
            });
    }
};

module.exports = {
    getRolesByPerfil
};