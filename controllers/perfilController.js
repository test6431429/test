const pool = require("../database/db");
const Perfil = require("../models/Perfil")
const {
    createNewPerfilQ,
    getAllPerfilesQ,
    getOnePerfilQ,
    updateOnePerfilQ,
    deleteOnePerfilQ,
} = require("../services/perfilService");

const createNewPerfil = async (req, res) => {
    const { id, nomebre } = req.body;
    try {
        const perfil = new Perfil({ id, nomebre });
        console.log('Model perfil: ', perfil);
        const createPerfil = await pool.query(createNewPerfilQ, [id, nomebre]);
        res.json(createPerfil.rows[0]);
        // res.status(201).send({ status: "OK", data: perfil.rows[0] });
    } catch (err) {
        console.error(err);
        res.status(err?.status || 500)
            .send({
                status: "Fallo",
                data: { error: "Alguno de estos campos puede estar vacio: 'nombre'", },
            });
    }
};

const getAllPerfiles = async (req, res) => {
    try {
        const allPerfiles = await pool.query(getAllPerfilesQ);

        res.json(allPerfiles.rows);
    } catch (err) {
        console.error(err.message);
    }
};

const getOnePerfil = async (req, res) => {
    const { id } = req.params;
    try {
        const perfil = await pool.query(getOnePerfilQ, [id]);

        // res.status(200).send(req.body); //OK
        res.json(perfil.rows[0]);
    } catch (err) {
        console.error(err.message);
    }
};

const updateOnePerfil = async (req, res) => {
    const { id } = req.params;
    const { nomebre } = req.body;
    try {
        const updatePerfil = await pool.query(updateOnePerfilQ, [nomebre, id]);
        res.json('Perfil actualizado');
    } catch (err) {
        console.error(err.message);
        res.status(err?.status || 500)
            .send({
                status: "Fallo",
                data: { error: "Alguno de estos campos puede estar vacio: 'nombre'", },
            });
    }
};

const deleteOnePerfil = async (req, res) => {
    const { id } = req.params;
    try {
        const deletePerfil = await pool.query(deleteOnePerfilQ, [id] );
        res.json('Perfil eliminado');
    } catch (err) {
        console.error(err.message);
        res.status(err?.status || 500)
            .send({
                status: "Fallo",
                data: { error: "Fallo", },
            });
    }

};

module.exports = {
    createNewPerfil,
    getAllPerfiles,
    getOnePerfil,
    updateOnePerfil,
    deleteOnePerfil,
};