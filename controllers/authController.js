const pool = require("../database/db");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const {
    queryFindUserByLogin,
    queryFindUserByCorreo,
    queryChangePassword
} = require("../services/authService");

const login = async (req, res) => {
    try {
        const { userLogin, clave } = req.body;

        const promisePool = pool.promise();
        const [user] = await promisePool.query(queryFindUserByLogin, [
            userLogin,
        ]);

        if (user.length <= 0) {
            return res.status(404).json({ message: "Usuario o contraseña errados" });
        }

        const passwordMatch = await bcrypt.compare(clave, user[0].clave);
        if (!passwordMatch) {
            return res.status(404).json({ message: "Usuario o contraseña errados" });
        }

        const token = await jwt.sign(
            {
                id: user[0].id,
                idPerfil: user[0].id_perfil,
                idEmpresa: user[0].id_empresa ? user[0].id_empresa : 0,
                empresa: user[0].empresa ? user[0].empresa : null,
                idSectorEmpresa: user[0].idSectorEmpresa ? user[0].idSectorEmpresa : null,
                rol: user[0].rol,
                nivelEducativo: user[0].nivel_educativo,
                correoEmpresarial: user[0].correo_empresarial,
                personalACargo: user[0].personal_a_cargo ? 'Si' : 'No',
                // correoPersonal: user[0].correo_personal,
                nombre: user[0].nombre,
                apellido: user[0].apellido,
            },
            process.env.ACCESS_TOKEN_SECRET,
            { expiresIn: "2h", subject: `${user[0].id}` }
        );

        return res.status(200).json({
            status: "success",
            success: true,
            message: "User logged in",
            token,
        });

    } catch (error) {
        return res.status(500).json({ message: "Something goes wrong" });
    }
};

const verifyCorreo = async (req, res) => {
    try {
        const { correo } = req.params;

        const promisePool = pool.promise();
        const [usuario] = await promisePool.query(queryFindUserByCorreo, [
            correo
        ]);

        if (usuario.length <= 0) {
            return res.status(404).json({
                status: "fail",
                success: false,
                message: "El correo no existe!!"
            });
        }

        return res.status(200).json({
            status: "success",
            success: true,
            message: "User logged in",
            usuario: usuario[0]
        });

    } catch (error) {
        return res.status(500).json({
            status: "fail",
            success: false,
            message: "Correo no pudo ser verificado!!",
        });
    }
}

const cambiarClave = async (req, res) => {
    try {
        const { idUsuario } = req.params;
        const { clave } = req.body;

        console.log('clave',clave)
        console.log('idUsuario',idUsuario)
        const salt = await bcrypt.genSalt(12);
        const hashedPassword = await bcrypt.hash(clave, salt);

        const promisePool = pool.promise();
        const [usuario] = await promisePool.query(queryChangePassword, [
            hashedPassword,
            idUsuario,
        ]);

        return res.status(200).json({
            status: "success",
            success: true,
            message: "Datos actualizados!!",
        });
    } catch (error) {
        return res.status(500).json({
            status: "fail",
            success: false,
            message: "Contraseña no pudo ser cambiada!!",
        });
    }

}

module.exports = {
    login,
    verifyCorreo,
    cambiarClave
};