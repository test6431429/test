const pool = require("../database/db");
const bcrypt = require("bcryptjs");
const Usuario = require("../models/usuario");
const {
  queryGetAllUsuarios,
  queryGetOneUsuario,
  queryUpdateUsuario,
  queryComparePassword,
  queryUpdatePassword,
  procedureUsuario,
} = require("../services/usuarioService");

const getAllUsuarios = async (req, res) => {
  try {
    const promisePool = pool.promise();

    const [rows, fields] = await promisePool.query(queryGetAllUsuarios);

    console.log(rows);
    res.json(rows);
  } catch (err) {
    console.error(err.message);
    return res.status(500).json({ message: "Something goes wrong" });
  }
};

const getOneUsuario = async (req, res) => {
  try {
    const { id } = req.params;

    const promisePool = pool.promise();
    const [rows] = await promisePool.query(queryGetOneUsuario, [id]);

    res.status(200).json(rows[0]);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Something goes wrong" });
  }
};

const usuarioCreate = async (req, res) => {
  const {
    idPerfil,
    idEmpresa,
    idRol,
    nombre,
    apellido,
    correoEmpresarial,
    correoPersonal,
    fechaNacimiento,
    genero,
    estadoCivil,
    nivelEducativo,
    antiguedad,
    personalACargo,
    modalidadTrabajo,
    fechaRegistro,
    userlogin,
    clave,
  } = req.body;
  try {
    const usuario = new Usuario({
      idPerfil,
      idEmpresa,
      idRol,
      nombre,
      apellido,
      correoEmpresarial,
      correoPersonal,
      fechaNacimiento,
      genero,
      estadoCivil,
      nivelEducativo,
      antiguedad,
      personalACargo,
      modalidadTrabajo,
      fechaRegistro,
      userlogin,
      clave,
    });

    const salt = await bcrypt.genSalt(12);
    const hashedPassword = await bcrypt.hash(clave, salt);

    const promisePool = pool.promise();
    const [rows] = await promisePool.query(procedureUsuario, [
      "c",
      null,
      idPerfil,
      idEmpresa,
      idRol,
      nombre,
      apellido,
      correoEmpresarial,
      correoPersonal,
      fechaNacimiento,
      genero,
      estadoCivil,
      nivelEducativo,
      antiguedad,
      personalACargo,
      modalidadTrabajo,
      fechaRegistro,
      userlogin,
      hashedPassword,
    ]);

    if (rows[0][0].result === 'success') {
      return res.status(200).json({
        status: "success",
        success: true,
        message: "Puedes iniciar sesión!!!",
      });
    } else {
      res.status(403).send({
        status: "Fallo",
        success: false,
        message: "No se pueden registrar mas usuarios!",
      });
    }
    // res.status(201).send({ status: "OK", data: perfil.rows[0] });
  } catch (err) {
    console.error(err);
    if (err.code == "ER_DUP_ENTRY") {
      res.status(err?.status || 500).send({
        status: "Fallo",
        data: {
          error:
            "El correo empresarial o el correo personal ya se encuentran registrados!",
        },
      });
    } else {
      res.status(err?.status || 500).send({
        status: "Fallo",
        data: { error: "No se pudo completar la acción!" },
      });
    }
  }
};

const usuarioEdit = async (req, res) => {
  const { id } = req.params;
  const {
    idRol,
    nombre,
    apellido,
    correoPersonal,
    nivelEducativo,
    personalACargo,
  } = req.body;
  try {
    const usuario = new Usuario({
      id,
      idRol,
      nombre,
      apellido,
      correoPersonal,
      nivelEducativo,
      personalACargo,
    });

    // console.log("Model usuario(Editar): ", usuario);

    const promisePool = pool.promise();
    const [rows] = await promisePool.query(queryUpdateUsuario, [
      idRol,
      nombre,
      apellido,
      nivelEducativo,
      personalACargo,
      id,
    ]);

    // console.log(rows);
    // res.json(rows);
    res.status(200).send(usuario);
  } catch (err) {
    console.error(err);
    res.status(err?.status || 500).send({
      status: "Fallo",
      data: { error: "Error en la base de datos" },
    });
  }
};

const changePassword = async (req, res) => {
  try {
    const { id } = req.params;
    const { claveActual, claveNueva } = req.body;

    const promisePool = pool.promise();
    const [userClave] = await promisePool.query(queryComparePassword, [id]);

    const passwordMatch = await bcrypt.compare(claveActual, userClave[0].clave);

    if (!passwordMatch) {
      return res.status(404).json({ message: "Contraseña actual errada!!" });
    }

    const salt = await bcrypt.genSalt(12);
    const hashedPassword = await bcrypt.hash(claveNueva, salt);

    const [user] = await promisePool.query(queryUpdatePassword, [hashedPassword, id]);

    return res.status(200).json({
      status: "success",
      success: true,
      message: "Su clave ha sido cambiada!!"
    });

  } catch (error) {
    console.log(error)
    return res.status(500).json({ success: false, message: "Something goes wrong" });
  }
}

module.exports = {
  getAllUsuarios,
  getOneUsuario,
  usuarioCreate,
  usuarioEdit,
  changePassword
};
