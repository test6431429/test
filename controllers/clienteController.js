const pool = require("../database/db");

const { queryUpdateEmpresa } = require("../services/clienteService");

const updateEmpresa = async (req, res) => {
    try {
        const { id } = req.params;

        const {
            nombre, pais, ciudad, direccion, sede, telefono, pagWeb
        } = req.body;

        const promisePool = pool.promise();

        const [rows] = await promisePool.query(queryUpdateEmpresa, [nombre, pais, ciudad, direccion, sede, telefono, pagWeb, id]);

        res.status(200).json({
            status: "success",
            success: true,
            message: "Datos actualizados!!",
        });
    } catch (err) {
        console.error(err.message);
        res.status(err?.status || 500)
            .json({
                status: "fail",
                success: false,
                message: "No se puedo actualizar los datos!!",
            });
    }
};

module.exports = {
    updateEmpresa
};