const pool = require("../database/db");

const { queryGetAreas } = require("../services/areaService");

const getAreas = async (req, res) => {
    try {
        const promisePool = pool.promise();

        const [rows] = await promisePool.query(queryGetAreas);

        res.json(rows);
    } catch (err) {
        res.status(err?.status || 500).json({
            status: "fail",
            success: false,
            message: "No se pudo leer los datos de la base dedatos!!",
        });
    }
};

module.exports = {
    getAreas
};