const pool = require("../database/db");

const {
    queryTotals,
    queryTotalSectorModalidad,
    queryTotalFactores,
    queryTablaMetricasAllegry,
    queryTablaMetricasCliente
} = require("../services/administradorService");

const getTotales = async (req, res) => {
    try {
        const promisePool = pool.promise();
        const [rows] = await promisePool.query(queryTotals);

        return res.status(200).json({
            status: "success",
            success: true,
            message: "Totales!",
            totales: rows[0]
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ success: false, message: "Something went wrong" });
    }
}

const getTotalSectorModalidad = async (req, res) => {
    try {
        const promisePool = pool.promise();
        const [rows] = await promisePool.query(queryTotalSectorModalidad);

        return res.status(200).json({
            status: "success",
            success: true,
            message: "Totales!",
            totalesSectorModalida: rows
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            status: "fail",
            success: false,
            message: "Algo fallo"
        });
    }
}

const getTotalFactores = async (req, res) => {
    try {
        const promisePool = pool.promise();
        const [rows] = await promisePool.query(queryTotalFactores);

        return res.status(200).json({
            status: "success",
            success: true,
            message: "Totales!",
            totalesFactores: rows
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            status: "fail",
            success: false,
            message: "Algo fallo"
        });
    }
}

const getTablaMetricasAllegry = async (req, res) => {
    try {
        const { idEmpresa } = req.params;
        const promisePool = pool.promise();
        const [rows] = await promisePool.query(queryTablaMetricasAllegry, [idEmpresa]);

        return res.status(200).json({
            status: "success",
            success: true,
            message: "Success!",
            tabla: rows
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ success: false, message: "Something went wrong" });
    }
}

const getTablaMetricasCliente = async (req, res) => {
    try {
        const { idEmpresa } = req.params;
        const promisePool = pool.promise();
        const [rows] = await promisePool.query(queryTablaMetricasCliente, [idEmpresa]);

        return res.status(200).json({
            status: "success",
            success: true,
            message: "Success!",
            tablaCliente: rows
        });
    } catch (error) {
        console.log(error)
        return res.status(500).json({ success: false, message: "Something went wrong" });
    }
}

module.exports = {
    getTotales,
    getTotalSectorModalidad,
    getTotalFactores,
    getTablaMetricasAllegry,
    getTablaMetricasCliente
}