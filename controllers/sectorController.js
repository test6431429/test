const pool = require("../database/db");

const { queryGetAllSectores } = require("../services/sectorService");

const getAllSectores = async (req, res) => {
    try {
        const promisePool = pool.promise();

        const [sectores] = await promisePool.query(queryGetAllSectores);

        return res.status(200).json(sectores);

        // return res.status(200).json({
        //     status: "success",
        //     success: true,
        //     message: "Retorno con suceso!!",
        //     sectores
        // });
    } catch (err) {
        console.error(err.message);
        res.status(err?.status || 500)
            .send({
                status: "Fallo",
                data: { error: "No se pudo leer los sectores de la base de datos", },
            });
    }
};

module.exports = {
    getAllSectores
};