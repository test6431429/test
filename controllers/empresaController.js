const bcrypt = require("bcryptjs");
const { v4: uuidv4 } = require('uuid');

const pool = require("../database/db");
const {
    procedureEmpresaByLink,
    queryGetEmpresasProceso,
    procedureEmpresa,
    queryGetEmpresaById,
    queryGetPaqueteEmpresa,
    queryUpdateEmpresa,
    procedurePreguntasFactorCustom,
    queryCreateFactorCustom,
    queryCreatePreguntaFactorCustom,
    queryUpdatePreguntaFCustom
} = require("../services/empresaService");

const getEmpresaByLink = async (req, res) => {
    try {
        const { link } = req.params;

        const promisePool = pool.promise();
        const [rows, fields] = await promisePool.query(procedureEmpresaByLink, [
            link,
        ]);

        if (rows.length <= 0) {
            return res.status(404).json({ message: "Link no registrado!!" });
        }
        res.json(rows[0][0]);
    } catch (error) {
        return res.status(500).json({ message: "Something goes wrong" });
    }
};

const getEmpresasProceso = async (req, res) => {
    try {
        const promisePool = pool.promise();
        const [allEmpresas] = await promisePool.query(queryGetEmpresasProceso);

        if (allEmpresas.length <= 0) {
            return res
                .status(404)
                .json({ success: true, message: "No hay empresas registradas!!" });
        }

        return res.status(200).json({
            status: "success",
            success: true,
            message: "Returning all empresas",
            allEmpresas,
        });
    } catch (error) {
        return res
            .status(500)
            .json({ success: false, message: "Something goes wrong" });
    }
};

const createEmpresa = async (req, res) => {
    try {
        const {
            nombre,
            nit,
            idSectorEmpresa,
            pais,
            ciudad,
            cantidadColaboradores,
            direccion,
            telefono,
            sede,
            numSucursales,
            pagWeb,
            nombreRepresentante,
            cargoRepresentante,
            emailRepresentante,
            telefonoRepresentante,
            nombreContacto,
            cargoContacto,
            emailContacto,
            telefonoContacto,
            // linkRegistroUser,
            usuarioEmpresa,
            clave,
        } = req.body;

        const salt = await bcrypt.genSalt(12);
        const hashedPassword = await bcrypt.hash(clave, salt);
        const link = uuidv4().substring(0, 13);

        const promisePool = pool.promise();
        const [empresaId] = await promisePool.query(procedureEmpresa, [
            'c',
            nombre,
            nit,
            idSectorEmpresa,
            pais,
            ciudad,
            cantidadColaboradores,
            direccion,
            telefono,
            sede,
            numSucursales,
            pagWeb,
            nombreRepresentante,
            cargoRepresentante,
            emailRepresentante,
            telefonoRepresentante,
            nombreContacto,
            cargoContacto,
            emailContacto,
            telefonoContacto,
            link,
            usuarioEmpresa,
            hashedPassword,
            null, null, null, null, null
        ]);

        console.log('result', empresaId[0])
        return res.status(200).json({
            status: "success",
            success: true,
            message: "Empresa guardada!",
            empresa: empresaId[0][0],
        });
    } catch (error) {
        console.log('ErrorCreate', error);
        if (error.code == "ER_DUP_ENTRY") {
            res.status(409).send({
                status: "fail",
                success: false,
                message: "El nit ya se encuentra registrado. Puede que existan otras campos que ya se encuentren registrados!!",
            });
        } else {
            return res.status(500).json({ success: false, message: "Something goes wrong" });
        }
    }
};

const setPaqueteEmpresa = async (req, res) => {
    try {
        const {
            idEmpresa,
            idPaquete,
            idArea,
            fechaInicio
        } = req.body;

        const promisePool = pool.promise();
        const [row] = await promisePool.query(procedureEmpresa, [
            'p',
            '', '', null, '', '', null, '', '', '', null, '', '', '',
            '', '', '', '', '', '', '', '', '',
            idEmpresa,
            idPaquete,
            idArea,
            fechaInicio,
            null
        ]);

        return res.status(200).json({
            status: "success",
            success: true,
            message: "Paquete generado!!",
        });

    } catch (error) {
        console.log('SetPaquete', error);
        return res.status(500).json({
            status: "fail",
            success: false,
            message: "Ocurrio algo!!",
        });

    }
}

const getEmpresaById = async (req, res) => {
    try {
        const { idEmpresa } = req.params;

        const promisePool = pool.promise();
        const [empresa] = await promisePool.query(queryGetEmpresaById, [
            idEmpresa,
        ]);

        return res.status(200).json({
            status: "success",
            success: true,
            message: "Read Empresa!",
            empresa: empresa[0],
        });

    } catch (error) {
        console.log('getEmpresaById', error);
        return res.status(500).json({
            status: "fail",
            success: false,
            message: "Ocurrio algo!!",
        });
    }
}

const getPaqueteEmpresa = async (req, res) => {
    try {
        const { idEmpresa } = req.params;

        const promisePool = pool.promise();
        const [empresaPaquete] = await promisePool.query(queryGetPaqueteEmpresa, [
            idEmpresa,
        ]);

        return res.status(200).json({
            status: "success",
            success: true,
            message: "Read empresa paquete!",
            empresaPaquete: empresaPaquete[0],
        });

    } catch (error) {
        console.log('getPaqueteEmpresa', error);
        return res.status(500).json({
            status: "fail",
            success: false,
            message: "Ocurrio algo!!",
        });
    }
}

const addFechaExtraCierre = async (req, res) => {
    try {
        const { idEmpresa, dias } = req.body;

        const promisePool = pool.promise();
        const [empresaPaquete] = await promisePool.query(procedureEmpresa, [
            'd',
            '', '', null, '', '', null, '', '', '', null, '', '', '',
            '', '', '', '', '', '', '', '', '',
            idEmpresa,
            null,
            null,
            null,
            dias
        ]);

        return res.status(200).json({
            status: "success",
            success: true,
            message: "new fecha cierre!",
            empresaPaquete: empresaPaquete[0][0],
        });
    } catch (error) {
        console.log('addFechaExtraCierre', error);
        return res.status(500).json({
            status: "fail",
            success: false,
            message: "Ocurrio algo!!",
        });
    }
}

const updateEmpresa = async (req, res) => {
    try {
        const { id } = req.params;
        const {
            nombre, idSectorEmpresa, nit, pais, ciudad, cantidadColaboradores, direccion, sede, numSucursales, pagWeb,
            nombreRepresentante, emailRepresentante, telefonoRepresentante, nombreContacto, emailContacto, telefonoContacto
        } = req.body;

        const promisePool = pool.promise();
        const [row] = await promisePool.query(queryUpdateEmpresa, [
            nombre, idSectorEmpresa, nit, pais, ciudad, cantidadColaboradores, direccion, sede, numSucursales, pagWeb,
            nombreRepresentante, emailRepresentante, telefonoRepresentante, nombreContacto, emailContacto, telefonoContacto,
            id
        ]);

        return res.status(200).json({
            status: "success",
            success: true,
            message: "Empresa Actualizada generado!!",
        });

    } catch (error) {
        console.log('updateEmpresa', error);
        return res.status(500).json({
            status: "fail",
            success: false,
            message: "Ocurrio algo!!",
        });
    }
}

const getPreguntasFactorCustom = async (req, res) => {
    try {
        const { idEmpresa } = req.params;

        const promisePool = pool.promise();
        const [response] = await promisePool.query(procedurePreguntasFactorCustom, [
            'get_preguntasFactorCustom',
            idEmpresa,
            null, null
        ]);

        return res.status(200).json({
            status: "success",
            success: true,
            message: "Read Factor Preguntas Custom!",
            factorPreguntaCustom: response[0],
        });

    } catch (error) {
        console.log('getFactorPreguntasCustom', error);
        return res.status(500).json({
            status: "fail",
            success: false,
            message: "Ocurrio algo!!",
        });
    }
}

const createFactorCustom = async (req, res) => {
    try {
        const { idEmpresa, nombre, descripcion } = req.body;

        const promisePool = pool.promise();
        const [response] = await promisePool.query(queryCreateFactorCustom, [
            idEmpresa,
            nombre,
            descripcion
        ]);

        return res.status(200).json({
            status: "success",
            success: true,
            message: "Nuevo factor creado!",
        });

    } catch (error) {
        console.log('createFactorCustom', error);
        return res.status(500).json({
            status: "fail",
            success: false,
            message: "Ocurrio algo!!",
        });
    }
}

const createPreguntaFactorCustom = async (req, res) => {
    try {
        const { idFactorCustom, titulo, pregunta } = req.body;

        const promisePool = pool.promise();
        const [response] = await promisePool.query(queryCreatePreguntaFactorCustom, [
            idFactorCustom, titulo, pregunta
        ]);

        return res.status(200).json({
            status: "success",
            success: true,
            message: "Nueva pregunta factor creada!",
        });

    } catch (error) {
        console.log('createPreguntaFactorCustom', error);
        return res.status(500).json({
            status: "fail",
            success: false,
            message: "Ocurrio algo!!",
        });
    }
}

const updatePreguntaFCustom = async (req, res) => {
    try {
        const { id } = req.params;
        const { titulo, pregunta, habilitado } = req.body;

        const promisePool = pool.promise();
        const [response] = await promisePool.query(queryUpdatePreguntaFCustom, [
            titulo, pregunta, habilitado, id
        ]);

        return res.status(200).json({
            status: "success",
            success: true,
            message: "Pregunta factor actualizada!",
        });

    } catch (error) {
        console.log('updatePreguntaFCustom', error);
        return res.status(500).json({
            status: "fail",
            success: false,
            message: "Ocurrio algo!!",
        });
    }
}

const setFechaPreguntaFCustom = async (req, res) => {
    try {
        const { idEmpresa, fecha, id } = req.body;

        console.log('setFechaPreguntaFCustom1', idEmpresa, fecha, id);

        const promisePool = pool.promise();
        const [response] = await promisePool.query(procedurePreguntasFactorCustom, [
            'guardar_fecha',
            idEmpresa,
            fecha, id
        ]);

        console.log('setFechaPreguntaFCustom2', response);

        if (response[0][0].result === 'Sin paquete') {
            return res.status(403).json({
                status: "fail",
                success: false,
                message: "Empresa no tiene paquete habilitado!!",
            });
        } else if (response[0][0].result === 'Fecha llena') {
            return res.status(403).json({
                status: "fail",
                success: false,
                message: "Fecha seleccionada se encuentra llena!!",
            });
        }

        return res.status(200).json({
            status: "success",
            success: true,
            message: "Fecha fijada!",
        });

    } catch (error) {
        console.log('setFechaPreguntaFCustomError', error);
        return res.status(500).json({
            status: "fail",
            success: false,
            message: "Ocurrio algo!!",
        });
    }
}

module.exports = {
    getEmpresaByLink,
    getEmpresasProceso,
    createEmpresa,
    setPaqueteEmpresa,
    getEmpresaById,
    getPaqueteEmpresa,
    addFechaExtraCierre,
    updateEmpresa,
    getPreguntasFactorCustom,
    createFactorCustom,
    createPreguntaFactorCustom,
    updatePreguntaFCustom,
    setFechaPreguntaFCustom
};
