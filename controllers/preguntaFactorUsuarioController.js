const pool = require("../database/db");

const {
    procedurePausaUsuario,
    queryGetPregunta,
    queryGetPreguntaCustom
} = require("../services/preguntaFactorUsuarioService");

const getPreguntasFactores = async (req, res) => {
    try {
        const { idUsuario } = req.params;

        const promisePool = pool.promise();
        const [rows, fields] = await promisePool.query(procedurePausaUsuario, [
            'get_preguntas_factores', null, null, null, idUsuario, null, null, null, null
        ]);

        if (rows.length <= 0) {
            return res.status(404).json({ message: "No hay preguntas de ningun factor hoy!!" });
        }
        res.json(rows[0]);
    } catch (error) {
        return res.status(500).json({ message: "Something goes wrong" });
    }
};

const getPausasFechas = async (req, res) => {
    try {
        const { idEmpresa, currentFecha } = req.body;

        const promisePool = pool.promise();
        const [rows] = await promisePool.query(procedurePausaUsuario, [
            'proceso_general', idEmpresa, currentFecha, null, null, null, null, null, null
        ]);

        if (rows.length <= 0) {
            return res.status(404).json({ message: "No hay procesos!!" });
        }
        res.json(rows[0]);
    } catch (error) {
        return res.status(500).json({ message: "Something goes wrong" });
    }
};

const getPreguntasFecha = async (req, res) => {
    try {

        const { idEmpresa, cualfecha, idPaqueteEmpresa, idUsuario } = req.body;

        const promisePool = pool.promise();
        const [rows] = await promisePool.query(procedurePausaUsuario, [
            'proceso_preguntas', idEmpresa, cualfecha, idPaqueteEmpresa, idUsuario, null, null, null, null
        ]);

        if (rows.length <= 0) {
            return res.status(404).json({ message: "No hay pregunta!!" });
        }
        res.json(rows[0]);
    } catch (error) {
        return res.status(500).json({ message: "Something goes wrong" });
    }
};

const getOnePregunta = async (req, res) => {
    try {
        const { id } = req.params;
        const promisePool = pool.promise();

        const [rows] = await promisePool.query(queryGetPregunta, [id]);

        if (rows.length <= 0) {
            return res.status(404).json({ message: "No hay pregunta!!" });
        }
        res.json(rows[0]);
    } catch (error) {
        return res.status(500).json({ message: "Something goes wrong" });
    }
}

const getOnePreguntaCustom = async (req, res) => {
    try {
        const { id } = req.params;
        const promisePool = pool.promise();

        const [rows] = await promisePool.query(queryGetPreguntaCustom, [id]);

        if (rows.length <= 0) {
            return res.status(404).json({ message: "No hay preguntas!!" });
        }
        res.json(rows[0]);
    } catch (error) {
        return res.status(500).json({ message: "Something goes wrong" });
    }
}

const saveRespuesta = async(req,res) => {
    try {
        const { accion, idUsuario, idCronograma, idPregunta, columnName, answerValue } = req.body;

        const promisePool = pool.promise();
        const [rows,] = await promisePool.query(procedurePausaUsuario, [
            accion, null, null, null, idUsuario, idCronograma, idPregunta, columnName, answerValue
        ]);
        res.json({message: 'Respuesta Guardada!!'});
    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: "Something goes wrong" });
    }
}

module.exports = {
    getPreguntasFactores,
    getPausasFechas,
    getPreguntasFecha,
    getOnePregunta,
    getOnePreguntaCustom,
    saveRespuesta
};