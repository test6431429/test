function Usuario ({
    id,
    idPerfil, //id_perfil
    idEmpresa, //id_empresa
    idRol, //id_rol
    nombre,
    apellido,
    correoEmpresarial, //correo_empresarial
    correoPersonal, //correo_personal
    fechaNacimiento, //fecha_nacimiento
    genero,
    estadoCivil, //estado_civil
    nivelEducativo, //nivel_educativo
    antiguedad,
    personalACargo, //personal_a_cargo
    modalidadTrabajo, //modalidad_trabajo
    fechaRegistro, //fecha_registro
    userlogin,
    clave,
    habilitado,
  }) {
      this.id = id;
      this.idPerfil= idPerfil;
      this.idEmpresa = idEmpresa;
      this.idRol = idRol;
      this.nombre = nombre;
      this.apellido = apellido;
      this.correoEmpresarial = correoEmpresarial;
      this.correoPersonal = correoPersonal;
      this.fechaNacimiento = fechaNacimiento;
      this.genero = genero;
      this.estadoCivil = estadoCivil;
      this.nivelEducativo = nivelEducativo;
      this.antiguedad = antiguedad;
      this.personalACargo = personalACargo;
      this.modalidadTrabajo = modalidadTrabajo;
      this.fechaRegistro = fechaRegistro;
      this.userlogin = userlogin;
      this.clave = clave;
      this.habilitado = habilitado;
  };

  module.exports = Usuario;
