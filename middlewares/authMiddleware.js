const jwt = require("jsonwebtoken");

module.exports = function authMiddleware(req, res, next) {
  // get token from headers

  const authToken = req.headers.authorization;

  // verify if token is filled in
  if (!authToken) {
    return res.status(401).json({
      status: "error",
      success: false,
      message: "Sin autorización!",
    });
  }

  // split token
  const token = authToken.split(" ")[1];

//   var payload = jwt.decode(token, process.env.ACCESS_TOKEN_SECRET);
//   if (payload.exp <= moment().unix()) {
//     return res.status(401).send({ status: "error", success: false, message: "La sesión ha expirado" });
//   }

  // console.log("token ", authToken);

  // verify if token is valid
  try {
    const { sub } = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    // const { sub } = jwt.verify(authToken, process.env.SECRET);

    // return user infos
    req.idUsuario = sub;

    return next();
  } catch (err) {
    return res.status(500).json({
      status: "error",
      success: false,
      message: err.message,
    });
  }
};
