const yup = require('yup');

const userSchema = yup.object({
    primerNombre: yup.string().required("Primer nombre obligatorio!"),
    primerApellido: yup.string().required("Primer apellido obligatorio"),
    loginUser: yup.string().email("Email incorrecto!").required(),
    clave: yup.string().min(4, "Clave debe ser min 4 caracteres").max(10, "Clave debe ser min 4 caracteres").required()
});

module.exports = userSchema;