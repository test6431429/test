const pool = require("../database/db");

module.exports = async function adminAllegryMiddleware(req, res, next) {
    // require id from headers
    const id = Number.parseInt(req.idUsuario);
  
    // check if user is admin
    const promisePool = pool.promise();

    const [user] = await promisePool.query('SELECT id_perfil idPerfil FROM usuario WHERE id=?', [id]);

  
    // if user is not admin, then return an error
    if (user[0].idPerfil !== 1) {
      return res.status(403).json({
        status: "error",
        success: false,
        message: "No tienes permiso para acceder!",
      });
    }
  
    // pass to next middleware
    return next();
  };
  