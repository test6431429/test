const validation = (schema) => async (req, res, next) => {

    const body = req.body;

    try {
        await schema.validate(body); // si no valida, salta al catch error
        next(); //-> Termina este middleware y continua (go futher with)
        return next();
    } catch (error) {
        return res.status(400).json({ error });
    }
};

module.exports = validation;