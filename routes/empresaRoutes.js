const express = require("express");
const router = express.Router();

const authMiddleware = require("../middlewares/authMiddleware");
const adminAllegryMiddleware = require("../middlewares/adminAllegryMiddleware");

const { 
    getEmpresaByLink,
    getEmpresasProceso,
    createEmpresa,
    setPaqueteEmpresa,
    getEmpresaById,
    getPaqueteEmpresa,
    addFechaExtraCierre,
    updateEmpresa,
    getPreguntasFactorCustom,
    createFactorCustom,
    createPreguntaFactorCustom,
    updatePreguntaFCustom,
    setFechaPreguntaFCustom
} = require("../controllers/empresaController");

router.get("/link/:link", getEmpresaByLink);
router.get("/proceso", authMiddleware, adminAllegryMiddleware, getEmpresasProceso);
router.post("/", authMiddleware, createEmpresa);
router.post("/paquete-empresa",authMiddleware, setPaqueteEmpresa);
router.get("/:idEmpresa", authMiddleware, getEmpresaById);
router.get("/paquete-empresa/:idEmpresa", authMiddleware, getPaqueteEmpresa);
router.put("/fecha-extra", authMiddleware, addFechaExtraCierre);
router.put("/:id", authMiddleware, updateEmpresa);
router.get("/pregunta-factor-custom/:idEmpresa", authMiddleware,  getPreguntasFactorCustom);
router.post("/factor-custom", authMiddleware, createFactorCustom);
router.post("/pregunta-factor-custom", authMiddleware, createPreguntaFactorCustom);
router.put("/pregunta-factor-custom/:id", authMiddleware, updatePreguntaFCustom);
router.post("/fecha-pregunta-factor-custom", authMiddleware, setFechaPreguntaFCustom);

module.exports = router;