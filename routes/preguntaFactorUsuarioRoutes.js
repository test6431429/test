const express = require("express");
const router = express.Router();

const authMiddleware = require("../middlewares/authMiddleware");

const {
    getPreguntasFactores,
    getPausasFechas, 
    getPreguntasFecha,
    getOnePregunta,
    getOnePreguntaCustom,
    saveRespuesta
} = require("../controllers/preguntaFactorUsuarioController")

router.get("/usuario/:idUsuario", authMiddleware, getPreguntasFactores);
router.post("/", authMiddleware, getPausasFechas);
router.post("/preguntas-fecha", authMiddleware, getPreguntasFecha);
router.get("/pregunta/:id", authMiddleware, getOnePregunta);
router.get("/pregunta-custom/:id", authMiddleware, getOnePreguntaCustom);
router.post("/save-respuesta", authMiddleware, saveRespuesta);

module.exports = router;