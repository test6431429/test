const express = require("express");
const router = express.Router();

const authMiddleware = require("../middlewares/authMiddleware");

const { 
    getPaquetes
} = require("../controllers/paqueteController")

router.get("/", authMiddleware, getPaquetes);

module.exports = router;