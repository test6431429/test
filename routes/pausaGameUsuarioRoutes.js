const express = require("express");
const router = express.Router();

const authMiddleware = require("../middlewares/authMiddleware");

const {
     getPausasActivas,
     getPausaGames,
     terminoPausa,
     getRespuestasGame,
     saveRespuestaCorrecta
} = require("../controllers/pausaGameUsuarioController")

router.get("/activas/usuario/:idUsuario", authMiddleware, getPausasActivas);
router.post("/activas", authMiddleware, terminoPausa);
router.get("/games/usuario/:idUsuario", authMiddleware, getPausaGames);
router.get("/games-respuestas/:idGame", authMiddleware, getRespuestasGame);
router.post("/send-respuesta", authMiddleware, saveRespuestaCorrecta);


module.exports = router;