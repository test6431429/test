const express = require("express");
const router = express.Router();

const authMiddleware = require("../middlewares/authMiddleware");

const { 
    getRandomFrase
} = require("../controllers/fraseController")

router.get("/", authMiddleware, getRandomFrase);

module.exports = router;