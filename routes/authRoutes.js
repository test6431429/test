const express = require("express");
const router = express.Router();

const {
    login,
    verifyCorreo,
    cambiarClave
} = require("../controllers/authController")

router.post("/login", login);
router.get("/verificar-correo/:correo", verifyCorreo);
router.put("/cambiar-clave/:idUsuario", cambiarClave);

module.exports = router;