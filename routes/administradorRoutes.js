const express = require("express");
const router = express.Router();

const authMiddleware = require("../middlewares/authMiddleware");
const adminAllegryMiddleware = require("../middlewares/adminAllegryMiddleware");
const adminClienteMiddleware = require("../middlewares/adminClienteMiddleware");

const {
    getTotales,
    getTotalSectorModalidad,
    getTotalFactores,
    getTablaMetricasAllegry,
    getTablaMetricasCliente
} = require("../controllers/administradorController")

router.get("/totales", authMiddleware, adminAllegryMiddleware, getTotales);
router.get("/totales-sector-modalidad", authMiddleware, adminAllegryMiddleware, getTotalSectorModalidad);
router.get("/totales-factores", authMiddleware, adminAllegryMiddleware, getTotalFactores);
router.get("/tabla-metricas-allegry/:idEmpresa", authMiddleware, adminClienteMiddleware, getTablaMetricasAllegry);
router.get("/tabla-metricas-cliente/:idEmpresa", authMiddleware, adminClienteMiddleware, getTablaMetricasCliente);

module.exports = router;