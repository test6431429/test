const express = require("express");
const router = express.Router();

const authMiddleware = require("../middlewares/authMiddleware");

const { 
    updateEmpresa
} = require("../controllers/clienteController")

router.put("/:id", authMiddleware, updateEmpresa);

module.exports = router;