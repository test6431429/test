const express = require("express");
const router = express.Router();

const authMiddleware = require("../middlewares/authMiddleware");

const { 
    getAreas
} = require("../controllers/areaController")

router.get("/", authMiddleware, getAreas);

module.exports = router;