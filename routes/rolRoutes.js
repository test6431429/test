const express = require("express");
const router = express.Router();

const { 
    getRolesByPerfil
} = require("../controllers/rolController")

router.get("/:idSector", getRolesByPerfil);

module.exports = router;