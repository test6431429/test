const express = require("express");
const router = express.Router();

const authMiddleware = require("../middlewares/authMiddleware");

const { 
    getAllUsuarios,
    getOneUsuario,
    usuarioCreate,
    usuarioEdit,
    changePassword
} = require("../controllers/usuarioController")

router.get("/", authMiddleware, getAllUsuarios);
router.get("/:id", authMiddleware, getOneUsuario);
router.post("/", usuarioCreate);
router.put("/:id", authMiddleware, usuarioEdit);
router.put("/clave/:id", changePassword);

module.exports = router;