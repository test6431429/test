const express = require("express");
const router = express.Router();

const { 
    getAllSectores
} = require("../controllers/sectorController")

router.get("/", getAllSectores);

module.exports = router;