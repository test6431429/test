const express = require("express");
const router = express.Router();

const { 
    createNewPerfil, 
    getAllPerfiles, 
    getOnePerfil,
    updateOnePerfil, 
    deleteOnePerfil 
} = require("../controllers/perfilController")

router.post("/", createNewPerfil);
router.get("/", getAllPerfiles);
router.get("/:id", getOnePerfil);
router.put("/:id", updateOnePerfil);
router.delete("/:id", deleteOnePerfil);

module.exports = router;