const express = require("express");
const router = express.Router();

const authMiddleware = require("../middlewares/authMiddleware");

const { getPermissionByPerfil } = require("../controllers/permissionController")

router.get("/:idPerfil", authMiddleware, getPermissionByPerfil);

module.exports = router;