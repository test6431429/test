const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')
require('dotenv').config();
const app = express();
// const port = 5000;
const port = process.env.PORT || 8080;

const pool = require("./database/db");
const validation = require("./middlewares/validationMiddleware");
const userSchema = require("./middlewares/validations/userValidation");

app.use(express.json()); // => req.body

app.use(cors({origin: '*'}))
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

// --  MIDDLEWARE QUE VA A SER LLAMADO EN CADA HTTP REQUEST
// app.use((req, res, next) => {
//     console.log("Middleware Called!");
//     next();
// });
// OR
// const checkUser = (req, res, next) => {
//     const name = req.body.name;

//     if (name === "WrongUser") res.json({ error: "Wrong User" });
//     else next();
// };
// app.post("/", checkUser, (req, res) => {
//     // res.json({ message: "sup!!"});
//     res.send("Puedes continuar")
// });
// --- END

// const v1PerfilRoutes = require("./routes/perfilRoutes");
// const v1PermissionRoutes = require("./routes/permissionRoutes");
const v1authRoutes = require("./routes/authRoutes");
const v1PermisoRoutes = require("./routes/permissionRoutes");
const v1AdminRoutes = require("./routes/administradorRoutes");
const v1SectorRoutes = require("./routes/sectorRoutes");
const v1RolRoutes = require("./routes/rolRoutes");
const v1AreaRoutes = require("./routes/areaRoutes");
const v1FraseRoutes = require("./routes/fraseRoutes");
const v1PaqueteRoutes = require("./routes/paqueteRoutes");
const v1UsurarioRoutes = require("./routes/usuarioRoutes");
const v1pausaGameUsurarioRoutes = require("./routes/pausaGameUsuarioRoutes");
const v1preguntaFactorUsuarioRoutes = require("./routes/preguntaFactorUsuarioRoutes");
const v1EmpresaRoutes = require("./routes/empresaRoutes");
const v1ClienteRoutes = require("./routes/clienteRoutes");

// app.use("/api/v1/perfiles", v1PerfilRoutes);
// app.use("/api/v1/permisos", v1PermissionRoutes);
app.use("/api/v1/auth", v1authRoutes);
app.use("/api/v1/permisos", v1PermisoRoutes);
app.use("/api/v1/admin", v1AdminRoutes);
app.use("/api/v1/sectores", v1SectorRoutes);
app.use("/api/v1/roles", v1RolRoutes);
app.use("/api/v1/areas", v1AreaRoutes);
app.use("/api/v1/frases", v1FraseRoutes);
app.use("/api/v1/paquetes", v1PaqueteRoutes);
app.use("/api/v1/usuarios", v1UsurarioRoutes);
app.use("/api/v1/pausas", v1pausaGameUsurarioRoutes);
app.use("/api/v1/proceso-usuario", v1preguntaFactorUsuarioRoutes);
app.use("/api/v1/empresas", v1EmpresaRoutes);
app.use("/api/v1/clientes", v1ClienteRoutes);

app.listen(port, () => {
    console.log(`App running on port ${port}.`)
});