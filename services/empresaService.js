const procedureEmpresaByLink =  "CALL sp_getEmpresaByLink(?);";

const queryGetEmpresasProceso = "SELECT E.id idEmpresa, E.nombre nombreEmpresa, E.id_sector_empresa idSectorEmpresa, " +
                                        "SE.nombre nombreSector, " +
                                        "(SELECT COUNT(id) " +
                                        "FROM usuario U " +
                                        "WHERE U.id_empresa = E.id) numUsuarios, " +
                                        "(SELECT CASE WHEN PE.fecha_extra_cierre IS NULL THEN (IF(NOW()>PE.fecha_cierre, 'Terminado', 'En proceso')) " +
                                                "ELSE (IF(CURDATE()>PE.fecha_extra_cierre, 'Terminado', 'En proceso')) END AS estado " +
                                        "FROM paquete_empresa PE " +
                                        "WHERE PE.id_empresa = E.id AND PE.habilitado=1) estado " +
                                "FROM empresa E " +
                                "INNER JOIN sector_empresa SE " +
                                "ON SE.id = E.id_sector_empresa;";

const procedureEmpresa =  "CALL sp_empresa(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

const queryGetEmpresaById = 'SELECT E.id, E.cantidad_colaboradores cantidadColaboradores, E.cargo_contacto cargoContacto, E.cargo_representante cargoRepresentante, ' +
                                    'E.ciudad, E.direccion, E.email_contacto emailContacto, E.email_representante emailRepresentante, E.fecha_registro fechaRegistro, ' +
                                    'E.id_sector_empresa idSectorEmpresa, E.link_registro_user linkRegistroUser, E.nit, E.nombre, E.nombre_contacto nombreContacto, ' +
                                    'E.nombre_representante nombreRepresentante, E.num_sucursales numSucursales, E.pag_web pagWeb, E.pais, E.sede, E.telefono, ' + 
                                    'E.telefono_contacto telefonoContacto, E.telefono_representante telefonoRepresentante, ' +
                                    'SE.nombre nombreSector, U.userlogin usuarioEmpresa ' +
                            'FROM empresa E ' +
                            'INNER JOIN sector_empresa SE ON SE.id = E.id_sector_empresa ' +
                            'INNER JOIN usuario U ON U.id_empresa = E.id AND U.id_perfil = 2 ' +
                            'WHERE E.id = ?;'

const queryGetPaqueteEmpresa = 'SELECT PE.id idPaqueteEmpresa, PE.id_empresa idEmpresa, ' +
                                        'PE.id_paquete idPaquete, P.nombre nombrePaquete, ' +
                                        'PE.id_area idArea, PE.fecha_inicio fechaInicio, ' +
                                        'PE.fecha_cierre fechaCierre, PE.fecha_extra_cierre fechaExtraCierre ' +
                                'FROM paquete_empresa PE ' +
                                'LEFT JOIN paquete P ' +
                                'ON P.id = PE.id_paquete ' +
                                'WHERE PE.habilitado = TRUE AND PE.id_empresa = ?;';

const queryUpdateEmpresa =  "UPDATE empresa " +
                             "SET nombre = ?, " + 
                                  "id_sector_empresa = ?, " +
                                   "nit = ?, " +
                                   "pais = ?, " +
                                   "ciudad = ?, " +
                                   "cantidad_colaboradores = ?, " +
                                   "direccion = ?, " +
                                   "sede = ?, " +
                                   "num_sucursales = ?, " +
                                   "pag_web = ?, " +
                                   "nombre_representante = ?, " +
                                   "email_representante = ?, " +
                                   "telefono_representante = ?, " +
                                   "nombre_contacto = ?, " +
                                   "email_contacto = ?, " +
                                   "telefono_contacto = ? " +
                              "WHERE id = ?;";


const procedurePreguntasFactorCustom = 'CALL sp_preguntaFactorCustom(?,?,?,?);';

const queryCreateFactorCustom = 'INSERT INTO factor_custom ' +
                                '(id_empresa, nombre, descripcion, habilitado) ' +
                                'VALUES ' +
                                '(?, ?, ?, TRUE);';

const queryCreatePreguntaFactorCustom = 'INSERT INTO pregunta_factor_custom ' +
                                        '(id_factor_custom, titulo, pregunta, habilitado) ' +
                                        'VALUES ' +
                                        '(?, ?, ?, TRUE);';

const queryUpdatePreguntaFCustom = 'UPDATE pregunta_factor_custom ' +
                                   'SET titulo = ?, ' +
                                        'pregunta = ?, ' +
                                        'habilitado = ? ' +
                                   'WHERE id = ?;'


module.exports = {
    procedureEmpresaByLink,
    queryGetEmpresasProceso,
    procedureEmpresa,
    queryGetEmpresaById,
    queryGetPaqueteEmpresa,
    queryUpdateEmpresa,
    procedurePreguntasFactorCustom,
    queryCreateFactorCustom,
    queryCreatePreguntaFactorCustom,
    queryUpdatePreguntaFCustom
};