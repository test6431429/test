const queryFindUserByLogin =  "SELECT U.id, U.id_perfil, U.id_empresa, " +
                                    "U.nombre, U.apellido, U.nivel_educativo, " +
                                    "U.correo_empresarial, U.correo_personal, " +
                                    "R.nombre rol, U.personal_a_cargo, " +
                                    "U.userlogin, U.clave, " +
                                    "E.nombre AS empresa, E.id_sector_empresa idSectorEmpresa " +
                                "FROM usuario AS U " +
                                "LEFT JOIN empresa AS E " +
                                "ON E.id = U.id_empresa " +
                                "LEFT JOIN rol as R " +
                                "ON R.id = U.id_rol " +
                                "WHERE userlogin = ?; ";

const queryFindUserByCorreo = "SELECT id, nombre, apellido " +
                              "FROM usuario " +
                              "WHERE correo_personal = ?"

const queryChangePassword = "UPDATE usuario " +
                            "SET clave = ? " +
                            "WHERE id = ?;"                             

module.exports = {
    queryFindUserByLogin,
    queryFindUserByCorreo,
    queryChangePassword
};