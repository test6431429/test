const queryGetAllUsuarios =  "SELECT * FROM usuario";
const queryGetOneUsuario =  "SELECT id, id_rol idRol, nombre, apellido, " + 
                                    "nivel_educativo nivelEducativo, personal_a_cargo personalACargo, " + 
                                    "correo_empresarial correoEmpresarial, correo_personal correoPersonal " + 
                            "FROM usuario WHERE id = ?;";
const queryUpdateUsuario =  "UPDATE usuario " +
                            "SET id_rol = ?, " + 
                                "nombre = ?, " +
                                "apellido = ?, " +
                                "nivel_educativo = ?, " +
                                "personal_a_cargo = ? " +
                            "WHERE id = ?;";
const queryComparePassword = "SELECT clave FROM usuario WHERE id = ?";
const queryUpdatePassword = "UPDATE usuario " +
                            "SET clave = ? " +
                            "WHERE id = ?;"

const procedureUsuario =  "CALL sp_usuario(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

module.exports = {
    queryGetAllUsuarios,
    queryGetOneUsuario,
    queryUpdateUsuario,
    queryComparePassword,
    queryUpdatePassword,
    procedureUsuario
};