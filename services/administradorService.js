const queryTotals = 'SELECT (SELECT COUNT(id) FROM empresa WHERE habilitado = TRUE) totalEmpresas, ' +
                            '(SELECT MAX(fecha_registro) FROM empresa WHERE habilitado = TRUE) ultimaEmpresa, ' +
                            '(SELECT COUNT(id) FROM usuario WHERE habilitado = TRUE AND id_perfil = 3) totalUsuarios, ' +
                            '(SELECT MAX(fecha_registro) FROM usuario WHERE habilitado = TRUE AND id_perfil = 3) ultimoUsuario, ' +
                            '(SELECT COUNT(id) FROM factor) totalFactores, ' +
                            '(SELECT COUNT(id) FROM pregunta_factor WHERE habilitado = TRUE) totalPreguntas;';

const queryTotalSectorModalidad = 'SELECT * ' +
                                'FROM ( ' +
                                        'SELECT \'sector\' tipo, COUNT(E.id) total, S.nombre ' +
                                        'FROM empresa E ' +
                                        'INNER JOIN sector_empresa S ' +
                                        'ON S.id = E.id_sector_empresa ' +
                                        'GROUP BY S.nombre ' +
                                        'UNION ALL ' +
                                        'SELECT \'modalidad\' tipo, COUNT(id) total, modalidad_trabajo nombre ' +
                                        'FROM usuario ' +
                                        'WHERE habilitado=TRUE AND id_perfil=3 ' +
                                        'GROUP BY modalidad_trabajo ' +
                                    ') TN;';

const queryTotalFactores = 'SELECT SUM(P.puntuacion) total, SUM(P.total_preguntas) totalPreguntas, P.id_factor idFactor, F.nombre ' +
                        'FROM puntuacion_factor_user P ' +
                        'LEFT JOIN factor F ' +
                        'ON F.id = P.id_factor ' +
                        'WHERE habilitado = TRUE ' +
                        'GROUP BY P.id_factor, F.nombre';

const queryTablaMetricasAllegry = 'SELECT  PFU.id_factor idFactor, F.nombre factor, ' +
                                        'SUM(PFU.puntuacion) puntajeAcumulado, SUM(PFU.total_preguntas) cantidadPreguntas, ' +
                                        '(SELECT COUNT(id) FROM pregunta_factor WHERE id_factor = PFU.id_factor) totalPreguntas, ' +
                                        '(SELECT COUNT(id_usuario) FROM puntuacion_factor_user WHERE id_factor = PFU.id_factor) totalPersonasContestaron, ' +
                                        '(SELECT COUNT(id)-1 FROM usuario WHERE id_empresa = PE.id_empresa) totalPersonas ' +
                                    'FROM puntuacion_factor_user PFU ' +
                                    'INNER JOIN paquete_empresa PE ' +
                                        'ON PE.id = PFU.id_paquete_empresa AND PE.habilitado = TRUE ' +
                                    'INNER JOIN factor F ' +
                                        'ON F.id = PFU.id_factor ' +
                                    'WHERE PE.id_empresa = ? ' +
                                    'GROUP BY PFU.id_factor, F.nombre;';

const queryTablaMetricasCliente = 'SELECT PC.id_factor_custom idFactorCustom, PC.titulo, PC.id_pregunta_fcustom idPreguntaFcustom, FC.nombre ' +
                                        ', SUM(puntuacion_custom) puntajeAcumulado, COUNT(PC.id_usuario) totalPersonasContestaron ' +
                                        ', (SELECT COUNT(id)-1 FROM usuario WHERE id_empresa = PE.id_empresa) totalPersonas ' +
                                    'FROM ( ' +
                                        'SELECT RPC.id_usuario, RPC.id_cronograma_pregunta_fcustom, PFC.id_factor_custom, PFC.titulo, ' +
                                            'RPC.id_pregunta_fcustom1 id_pregunta_fcustom, puntuacion_custom1 puntuacion_custom ' +
                                        'FROM respuesta_pregunta_fcustom AS RPC ' +
                                        'INNER JOIN pregunta_factor_custom AS PFC ON RPC.id_pregunta_fcustom1 = PFC.id ' +
                                        'UNION ALL ' +
                                        'SELECT RPC.id_usuario, RPC.id_cronograma_pregunta_fcustom, PFC.id_factor_custom, PFC.titulo, ' +
                                        'RPC.id_pregunta_fcustom2 id_pregunta_fcustom, puntuacion_custom2 puntuacion_custom ' +
                                        'FROM respuesta_pregunta_fcustom AS RPC ' +
                                        'INNER JOIN pregunta_factor_custom AS PFC ON RPC.id_pregunta_fcustom2 = PFC.id ' +
                                        'UNION ALL ' +
                                        'SELECT RPC.id_usuario, RPC.id_cronograma_pregunta_fcustom, PFC.id_factor_custom, PFC.titulo, ' +
                                        'RPC.id_pregunta_fcustom3 id_pregunta_fcustom, puntuacion_custom3 puntuacion_custom ' +
                                        'FROM respuesta_pregunta_fcustom AS RPC ' +
                                        'INNER JOIN pregunta_factor_custom AS PFC ON RPC.id_pregunta_fcustom3 = PFC.id ' +
                                    ') PC ' +
                                    'INNER JOIN factor_custom FC ON FC.id = PC.id_factor_custom ' +
                                    'INNER JOIN cronograma_pregunta_fcustom CPC ON CPC.id = PC.id_cronograma_pregunta_fcustom ' +
                                    'INNER JOIN paquete_empresa PE ON PE.id = CPC.id_paquete_empresa AND PE.habilitado = TRUE ' +
                                    'WHERE PE.id_empresa = ? ' +
                                    'GROUP BY PC.id_factor_custom, PC.titulo, PC.id_pregunta_fcustom, FC.nombre;'
                        
module.exports = {
    queryTotals,
    queryTotalSectorModalidad,
    queryTotalFactores,
    queryTablaMetricasAllegry,
    queryTablaMetricasCliente
}