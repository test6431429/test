const createNewPerfilQ = "INSERT INTO perfil (id, nomebre) VALUES ($1, $2) RETURNING *"

const getOnePerfilQ = "SELECT * FROM perfil WHERE id = $1";

const getAllPerfilesQ =  "SELECT * FROM perfil";

const updateOnePerfilQ = "UPDATE perfil SET nomebre = $1 WHERE id = $2";

const deleteOnePerfilQ = "DELETE FROM perfil WHERE id = $1";

module.exports = {
    createNewPerfilQ,
    getOnePerfilQ,
    getAllPerfilesQ,
    updateOnePerfilQ,
    deleteOnePerfilQ,
};