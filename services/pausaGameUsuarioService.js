const procedurePausaGameUsuario =  "CALL sp_pausaGameUsuario(?,?);";

const queryGetPregunta = "SELECT * FROM respuesta_game WHERE id_game = ?;";

module.exports = {
    procedurePausaGameUsuario,
    queryGetPregunta
};