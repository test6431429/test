const procedurePausaUsuario =  "CALL sp_preguntaFactorUsuario(?,?,?,?,?,?,?,?,?);";

const queryGetPregunta = "SELECT * FROM pregunta_factor WHERE id = ?;";
const queryGetPreguntaCustom = "SELECT * FROM pregunta_factor_custom WHERE id = ?;";

module.exports = {
    procedurePausaUsuario,
    queryGetPregunta,
    queryGetPreguntaCustom
};