const queryUpdateEmpresa = "UPDATE empresa " +
                            "SET nombre = ?, " +
                                "pais = ?, " +
                                "ciudad = ?, " +
                                "direccion = ?, " +
                                "sede = ?, " +
                                "telefono = ?, " +
                                "pag_web = ? " +
                            "WHERE id = ?;";

module.exports = {
    queryUpdateEmpresa
};