const getPermissionsByPerfilQ =
  "SELECT M.*, PPM.crear, PPM.leer, PPM.actualizar, PPM.borrar " +
  "FROM menu AS M " +
  "INNER JOIN permisos_perfil_menu AS PPM " +
  "ON PPM.id_menu = M.id " +
  "INNER JOIN perfil AS P " +
  "ON P.id = PPM.id_perfil " +
  "WHERE PPM.id_perfil = $1 " +
  "ORDER BY M.id";

const queryGetPermissionsByPerfil =
  "SELECT M.menu_type, M.nombre, M.icono, M.ruta, M.rownum, " + 
          "PPM.crear, PPM.leer, PPM.actualizar, PPM.borrar " +
  "FROM menu AS M " +
  "INNER JOIN permiso_perfil_menu AS PPM " +
    "ON PPM.id_menu = M.id " +
  "INNER JOIN perfil AS P " +
    "ON P.id = PPM.id_perfil " +
  "WHERE PPM.id_perfil = ? " +
  "ORDER BY M.rownum";

module.exports = {
  getPermissionsByPerfilQ,
  queryGetPermissionsByPerfil
};
